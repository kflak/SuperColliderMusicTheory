KF.start;

// post all built-in scales
Scale.directory;

(
// var scale = Scale.major;
var scale = Scale.minor;
var size = scale.size;

Pdef(\scaleIntervals,
    Pbind(
        \instrument, \sine,
        \scale, scale,
        \degree, Pseq([[0], (1..size)].lace(size*2).mirror, inf),
        \root, 0
    )
).play;
)
